import numpy as np

#dtype puede definir el tipo del array

a = np.array([1,2,3,4,5], dtype='i')
b = np.array([1,6,3,4,9])

#dimensions
# los arrays dentro de las dimensiones deben de tener las misma cantidad de elementos

d = np.array([[1,2,3], [4,5,6], [7,8,9]])

print(d.ndim)
print(d[0,2])
print(d.shape)
print(d.shape[1])

long_D = np.arange(20,100,2)
print(long_D)

random_D = np.random.permutation(np.arange(10))
print(random_D)

source = np.random.rand(1000)
import matplotlib.pyplot as plt

plt.hist(source, bins=100)

rearrange = np.arange(100).reshape(4,25)
print (rearrange)
print( rearrange.shape)