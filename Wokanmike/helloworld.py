msg = "Hello, Python"

print(type(msg))

print(dir(msg))

print( msg[-1])

print (f"pedo de  {msg}")

inTer = input("Console Input us a string: ")
int(inTer)
print(inTer)

new_list = list((1, 2, 3, 4))
print (new_list)

el_rango = list(range(1,10))
print (dir(el_rango))

#la función in puede ayudar a ver si un elemento existe importante para hacer el webscrapping
print( 2 in el_rango)
print( "Down" in msg)

el_rango.extend([1, 2])

print(el_rango)
print(el_rango.count(5))
#con insert se puede incrustar en una posición determinada un elemento
el_rango.insert(9, 10)
#elimina el ultipo elemento dento de una lista sin tener un parametro o si le pones un indice borra el indice
el_rango.pop(1)
#selecciona el elemento en especifico
el_rango.remove(8)
print(el_rango)
#ordena a las listas, con 'reverse=True' lo hace al revez
el_rango.sort(reverse=True)
print(el_rango)


# el elemento mpinimo de una tupla es (1, ) si no se convierte en otro tipo de dato
the_inmutable = tuple(("hola", "adios", "drancer"))
print(the_inmutable)

#La función ´del´ puede borrar una variable

no_dictionary = {'almost', 'a', 'json', 'without', 'an', 'index'}
print ('json' in no_dictionary)
no_dictionary.add('element')
no_dictionary.remove('almost')


#this is a dictonarie

dition = {
    "stock": "AAl",
    "price": "45",
    "quantity": 3
  }

print(dition.keys())
print(dition.items())

#conditional    if x == y:    elseif =   elif     operadores lógicos en lugar de && = and  y || = or  y ! = not 

items = ['apple', 'onion', 'pineapple', 'carrot', 'banana']

for item in items:
    if item == 'carrot' or item == 'onion':
        print(f"{item} is not a fruit")
        #or can be a continue
        break
    print(item)

#while i == x:  can turn infinite

def hell(name= "Default value"):
    print(f'estructura básica de una función {name}')
    return 'the return of the function'

hell('Mike')
print (hell())

#funcion lambda funciones de una sola linea
add = lambda p1, p2: p1 + p2

print(add(3,20))

#creación de módulos
#módulos intrinsecos de python
from datetime import date, timedelta

print(timedelta(minutes=180))
print(date.today())

#para importar módulos propios con import -nombre del archivo-  desde el archivo fuente no es necesario definir los módulos a exportar

#funciones asyncronas

# el tema con python es que es un lenguaje sincrono e incluso la mayoria de las librerias lo son, por eso es imporante tener el detalle de que las librerias o modulos que ocupe
# primero será necesario verificar que sea asycrono y seguir la esturctura para no parar el thread prncipal
#ya hay algunos modulos que son asyncronos como aiohttp, aiopg, aiomysql, aiofiles

import asyncio
import time

async def say_after(delay, text):
    await asyncio.sleep(delay)
    print(text)

async def main():
    print(f"started at {time.strftime('%X')}")
    t1 = say_after(1, 'wokan')
    t2 = say_after(2, 'mike')

    await asyncio.gather(t1, t2)

    print(f"finished at {time.strftime('%X')}")


asyncio.run(main())

# promesas, es posible ahcer un lenguaje syncrono asyncrono, y evitar tener un codigo totalmente de uno, con la propiedad de generar multiples theads con las librerias de threading
# y la libreria de concurrrent django channels
